#include <EEPROM.h>
#include <SoftwareSerial.h>


#define BUZ 7
#define MAX 10

#define CONTROL_MODE 0
#define ADD_MODE 1
#define DELETE_MODE 2
#define STRT_ADDR 2
#define ID_LENGTH 14

SoftwareSerial RFID(2, 3); // RX and TX
int i;
String rfid = "";
String newtag = "";

String authorized[MAX];
uint8_t mode;
long lastBipAddMode=0;

void setup() {
  RFID.begin(9600); // start serial to RFID reader
  Serial.begin(9600); // start serial to PC
  pinMode(BUZ, OUTPUT); 
  for(int i=0; i < MAX; i++) authorized[i]="";
  
  initId();

  if(EEPROM.read(1) < 5) {
    tone(BUZ,1200, 100);
    EEPROM.write(0, EEPROM.read(0) + 1);
  } else {
    EEPROM.write(0, 0);
  }
  if(EEPROM.read(0) > 3) {
    resetSound();
    mode=ADD_MODE;
    for (int i = 0 ; i < EEPROM.length() ; i++) {
      EEPROM.write(i, 0);
    }
    
  }
  mode = numberOfSlotTaken() == 0 ? ADD_MODE : CONTROL_MODE;
}

void loop() {
  switch(mode) {
    case CONTROL_MODE: controlMode(); break;
    case ADD_MODE: addMode(); break;
    case DELETE_MODE: break;
    default:controlMode();
  }
  EEPROM.write(1,String(millis()).length());
  
  delay(500);
  RFID.flush();
  if (!newtag.equals("") && RFID.available() > 0) {
    while(RFID.read()>0){
    }
  }
  newtag="";
}

String readId() {
  if (RFID.available() > 0){
    for (int z = 0 ; z < ID_LENGTH ; z++) {
      delay(1);
      char c= RFID.read();
      if(c==-1) return "";
      newtag += c;
    }
    Serial.println("Read "+newtag);
    return newtag;
    RFID.flush();
  }
  return "";
}

void controlMode() {
   String id = readId();
   
   if(!id.equals("")) {
    if(isPresent(id)) {
      Serial.println("Accept " + id);
      acceptSound();
    } else {
      Serial.println("Reject " + id);
      rejectSound();
    }
   }
}

void addMode() {
  String id = readId();
  if(millis() - lastBipAddMode >1000) {
    tone(BUZ,1000, 20);
    lastBipAddMode = millis();
  }
  if(!id.equals("")) {
    add(id);
    if(numberOfSlotTaken() > 0) mode = CONTROL_MODE;
  }
  
 
  
  
}

void acceptSound() {
  tone(BUZ, 600, 20);
  delay(60);
  tone(BUZ, 800, 20);
  delay(60);
  tone(BUZ, 1400, 20);
  delay(40);
  
}

void resetSound() {
  tone(BUZ, 1400, 20);
  delay(60);
  tone(BUZ, 1300, 20);
  delay(60);
  tone(BUZ, 1400, 20);
  delay(60);
  tone(BUZ, 1300, 20);
  delay(60);
  
}

void rejectSound() {
  tone(BUZ, 100, 80);
  delay(140);
  tone(BUZ, 100, 300);
  delay(350);
}

void addSound() {
  tone(BUZ, 900, 80);
  delay(140);
  tone(BUZ, 800, 100);
  delay(140);
  tone(BUZ, 1100, 200);
  delay(500);
}


void add(String id) {
  int slot = findEmptySlot();
  if(slot ==-1 || isPresent(id)) {
    rejectSound();
  } else {
    authorized[slot]=id;
    addSound();
    for(int j=0; j<authorized[slot].length();j++) {
      EEPROM.write(slot * ID_LENGTH + j + STRT_ADDR, authorized[slot][j]);
    }
  }
}

void initId() {
  Serial.println("available ID");
  for(int i=0; i<MAX; i++) {
    String id = "";
    for(int j=0; j<ID_LENGTH; j++) {
      id += (char)EEPROM.read(i*ID_LENGTH+j + STRT_ADDR);
    }
    authorized[i]=id;
    Serial.println(id);
  }
}

int findEmptySlot() {
  String cmp = "";
  for(int i=0; i<MAX;i++) {
    if(authorized[i].equals("")||authorized[i].equals(cmp)) {
      return i;  
    }
      
  }
  return -1;
}

int numberOfSlotTaken() {
   String cmp = "";
   int slot=0;
   for(int i=0; i<MAX;i++) {
      if(!authorized[i].equals("")&&!authorized[i].equals(cmp)) {
      slot++;  
    }
      
  }
  return slot;
}

bool isPresent(String id) {
   for(int i=0; i<MAX;i++) {
      if(authorized[i].equals(id)) {
        return true;
      }
   }
   return false;
}
